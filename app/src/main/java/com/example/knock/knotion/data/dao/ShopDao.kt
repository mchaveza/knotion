package com.example.knock.knotion.data.dao

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import com.example.knock.knotion.data.entity.ShopEntity

@Dao
interface ShopDao {

    @Query("SELECT * from shop_table ORDER BY shop_name ASC")
    fun getAllStores(): LiveData<List<ShopEntity>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun saveStore(store: ShopEntity)

}