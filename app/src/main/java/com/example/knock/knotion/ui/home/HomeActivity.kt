package com.example.knock.knotion.ui.home

import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v7.widget.SearchView
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.AutoCompleteTextView
import android.widget.EditText
import android.widget.ImageView
import com.example.knock.knotion.R
import com.example.knock.knotion.ui.base.BaseActivity
import com.jakewharton.rxbinding.widget.RxTextView
import kotlinx.android.synthetic.main.activity_base.*
import rx.android.schedulers.AndroidSchedulers
import java.util.concurrent.TimeUnit

class HomeActivity : BaseActivity() {

    private var showIcon = false
    private val homeFragment by lazy { HomeFragment() }

    override fun getFragment(): Fragment = homeFragment

    fun changeToolbarTitle(title: String) {
        this.base_toolbar.title = title
        this.base_toolbar.setTitleTextColor(ContextCompat.getColor(this, R.color.icons))
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_product_search, menu)
        return showIcon
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.menu_search -> {
                initSearchView()
                item.expandActionView()
            }
            else -> {
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun initSearchView() {
        val searchView = this.base_toolbar.menu.findItem(R.id.menu_search)?.actionView as SearchView

        searchView.isSubmitButtonEnabled = false

        val closeButton = searchView.findViewById(R.id.search_close_btn) as ImageView
        closeButton.setImageResource(R.drawable.ic_close)

        val txtSearch = searchView.findViewById<View>(android.support.v7.appcompat.R.id.search_src_text) as EditText
        txtSearch.hint = getString(R.string.search_hint)
        txtSearch.setHintTextColor(ContextCompat.getColor(this, R.color.icons))
        txtSearch.setTextColor(ContextCompat.getColor(this, R.color.icons))

        val searchTextView =
            searchView.findViewById<View>(android.support.v7.appcompat.R.id.search_src_text) as AutoCompleteTextView
        setOnActionSearchListener(searchTextView)
    }

    private fun setOnActionSearchListener(searchView: AutoCompleteTextView) {
        RxTextView.afterTextChangeEvents(searchView)
            .debounce(1, TimeUnit.SECONDS)
            .observeOn(AndroidSchedulers.mainThread())
            .filter {
                it.editable()?.isNotEmpty()
            }
            .subscribe({
                homeFragment.performSearching(it.editable().toString())
            }, {
                Log.e("Error", it.message, it)
            })
    }

    fun showSearchIcon(show: Boolean) {
        showIcon = show
        invalidateOptionsMenu()
    }

}