package com.example.knock.knotion.ui.base

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import com.example.knock.knotion.KnotionApplication
import com.example.knock.knotion.R
import kotlinx.android.synthetic.main.activity_base.*

abstract class BaseActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        this.overridePendingTransition(R.anim.left_in, R.anim.left_out)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_base)

        initView()
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.base_container, getFragment())
            .commitAllowingStateLoss()

    }

    open fun initView() {
        initializeDagger()
        prepareSupportActionBar()
    }

    private fun prepareSupportActionBar() {
        this.base_toolbar.title = ""
        setSupportActionBar(this.base_toolbar)
        /*supportActionBar?.setDisplayHomeAsUpEnabled(true)
        this.base_toolbar.navigationIcon = ContextCompat.getDrawable(this@BaseActivity, R.drawable.ic_back)
        this.base_toolbar.setNavigationOnClickListener {
            onBackPressed()
        }*/
    }

    private fun initializeDagger() {
        KnotionApplication.getApplicationComponent().inject(this)
    }



    abstract fun getFragment(): Fragment
}