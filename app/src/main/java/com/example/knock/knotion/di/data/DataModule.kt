package com.example.knock.knotion.di.data

import android.arch.persistence.room.Room
import android.content.Context
import com.example.knock.knotion.data.dao.ShopDao
import com.example.knock.knotion.di.database.NapifyDatabase
import dagger.Module
import dagger.Provides

@Suppress("unused")
@Module
object DataModule {

    private const val DATABASE_NAME = "NapifyDB"

    @Provides
    @JvmStatic
    internal fun providesNapifyDatabase(context: Context): NapifyDatabase =
        Room.databaseBuilder(context.applicationContext, NapifyDatabase::class.java, DATABASE_NAME).build()

    @Provides
    @JvmStatic
    internal fun providesShopDao(database: NapifyDatabase): ShopDao =
        database.shopDao()

}