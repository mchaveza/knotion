package com.example.knock.knotion.ui.products

import android.arch.lifecycle.Observer
import android.support.v7.widget.GridLayoutManager
import android.view.View
import android.widget.AdapterView
import com.example.knock.knotion.KnotionApplication
import com.example.knock.knotion.R
import com.example.knock.knotion.ui.base.BaseFragment
import com.example.knock.knotion.ui.products.adapter.FilterAdapter
import com.example.knock.knotion.ui.products.adapter.ProductAdapter
import com.example.knock.knotion.ui.views.BaseView
import com.example.knock.knotion.viewmodels.ProductFilter
import com.example.knock.knotion.viewmodels.ProductViewModel
import com.ia.mchaveza.kotlin_library.gone
import com.ia.mchaveza.kotlin_library.visible
import kotlinx.android.synthetic.main.fragment_product.*
import javax.inject.Inject

class ProductFragment : BaseFragment(), BaseView {

    @Inject
    lateinit var mProductViewModel: ProductViewModel

    private val productAdapter by lazy { ProductAdapter() }
    private val filterAdapter by lazy { FilterAdapter() }

    override fun getLayout(): Int =
        R.layout.fragment_product

    override fun initView() {
        super.initView()
        setupViewModel()
        setupRecycler()
        setupFilter()
        getInitialProducts()
    }

    private fun setupFilter() {
        this.product_filter.adapter = filterAdapter
        this.product_filter.onItemSelectedListener = object : AdapterView.OnItemClickListener,
            AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                when (position) {
                    0 -> {
                        mProductViewModel.sortProductList(ProductFilter.Name)
                    }
                    1 -> {
                        mProductViewModel.sortProductList(ProductFilter.Price)
                    }
                }
            }

            override fun onItemClick(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
            }
        }
    }

    private fun setupViewModel() {
        mProductViewModel.setView(this)
    }

    private fun setupRecycler() {
        this.product_list.adapter = productAdapter
        this.product_list.layoutManager = GridLayoutManager(activity!!, 2)
        this.product_list.hasFixedSize()
    }

    override fun initializeDagger() {
        super.initializeDagger()
        KnotionApplication.getApplicationComponent().inject(this)
    }

    private fun getInitialProducts() {
        performSearching("smartphones")
    }

    override fun showLoading() {
        this.loading_container.visible()
    }

    override fun hideLoading() {
        this.loading_container.gone()
    }

    fun performSearching(query: String) {
        mProductViewModel.getProducts(query).observe(this, Observer { productResponse ->
            productResponse?.let {
                if (it.isNotEmpty()) {
                    productAdapter.setProducts(it)
                    showEmptyState(false)
                } else {
                    showEmptyState(true)
                }
            }
        })
    }

    private fun showEmptyState(show: Boolean) {
        if (show) {
            this.empty_state_container.visible()
        } else {
            this.empty_state_container.gone()
        }
    }

}