package com.example.knock.knotion.data.entity

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.support.annotation.NonNull

@Entity(tableName = "shop_table")
data class ShopEntity(

    @PrimaryKey(autoGenerate = true)
    val id: Int = 0,

    @NonNull
    @ColumnInfo(name = "shop_name")
    val name: String,

    @NonNull
    @ColumnInfo(name = "shop_image")
    val image: String,

    @NonNull
    @ColumnInfo(name = "shop_address")
    val address: String
)