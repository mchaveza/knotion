package com.example.knock.knotion.di.data

import com.example.knock.knotion.data.net.NetProductEntity
import com.example.knock.knotion.data.net.NetSeriesEntity
import com.example.knock.knotion.data.net.service.ProductService
import com.example.knock.knotion.data.net.service.SeriesService
import com.example.knock.knotion.data.repository.ProductsRepository
import com.example.knock.knotion.data.repository.SeriesRepository
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
class DataNetModule(private val baseUrl: String) {

    private val logging: HttpLoggingInterceptor = HttpLoggingInterceptor()
    private val httpClient: OkHttpClient.Builder
    private val gson: Gson

    init {
        logging.level = HttpLoggingInterceptor.Level.BODY

        httpClient = OkHttpClient.Builder()
        httpClient.readTimeout(30, TimeUnit.SECONDS)
        httpClient.connectTimeout(30, TimeUnit.SECONDS)
        httpClient.writeTimeout(15, TimeUnit.SECONDS)
        httpClient.addInterceptor(logging)

        gson = GsonBuilder().setLenient().create()
    }

    @Provides
    @Singleton
    fun providesDataConfiguration(): DataConfiguration = DataConfiguration(baseUrl)

    @Provides
    @Singleton
    fun providesRetrofit(dataConfiguration: DataConfiguration): Retrofit =
        Retrofit.Builder()
            .baseUrl(dataConfiguration.getBaseUrl())
            .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create(gson))
            .client(httpClient.build())
            .build()

    @Provides
    @Singleton
    fun providesProductRepository(retrofit: Retrofit): ProductsRepository =
        NetProductEntity(retrofit.create(ProductService::class.java))

    @Provides
    @Singleton
    fun providesSeriesRepository(retrofit: Retrofit): SeriesRepository =
        NetSeriesEntity(retrofit.create(SeriesService::class.java))
}