package com.example.knock.knotion.data.entity

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.support.annotation.NonNull

@Entity(tableName = "product_table")
data class ProductEntity(

    @PrimaryKey(autoGenerate = true)
    val id: Int = 0,

    @NonNull
    @ColumnInfo(name = "product_name")
    val name: String,

    @NonNull
    @ColumnInfo(name = "product_description")
    val description: String,

    @NonNull
    @ColumnInfo(name = "product_price")
    val price: String,

    @NonNull
    @ColumnInfo(name = "product_image")
    val image: String

)