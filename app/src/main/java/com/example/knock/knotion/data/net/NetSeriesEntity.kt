package com.example.knock.knotion.data.net

import com.example.knock.knotion.data.net.service.SeriesService
import com.example.knock.knotion.data.repository.SeriesRepository
import com.example.knock.knotion.ui.home.model.SeriesResponse
import rx.Observable

class NetSeriesEntity(private val seriesService: SeriesService) : SeriesRepository {

    override fun getProducts(): Observable<SeriesResponse> =
        seriesService.getProducts()
            .flatMap {
                if (it.isSuccessful) {
                    Observable.just(it.body())
                } else {
                    Observable.error(Throwable(it.message()))
                }
            }

}