package com.example.knock.knotion.ui.home.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.request.RequestOptions
import com.example.knock.knotion.R
import com.example.knock.knotion.ui.home.model.ResultsItem
import com.ia.mchaveza.kotlin_library.loadUrl
import java.util.*

class SeriesAdapter : RecyclerView.Adapter<SeriesAdapter.SeriesViewHolder>() {

    private var mInflater: LayoutInflater? = null
    private var mStores: List<ResultsItem> = ArrayList()
    private var mListener: SeriesCallback? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SeriesViewHolder {
        mInflater = LayoutInflater.from(parent.context)
        val itemView = mInflater?.inflate(R.layout.item_series, parent, false)
        return SeriesViewHolder(itemView!!)
    }

    override fun getItemCount(): Int =
        mStores.size

    override fun onBindViewHolder(holder: SeriesViewHolder, position: Int) = with(holder) {
        val current = mStores[position]
        val context: Context = name?.context!!
        this.name?.text = current.name
        this.image?.loadUrl(current.image.imageUrl, RequestOptions().fitCenter())
        this.numberOfEpisodes?.text =
                String.format(context.getString(R.string.number_of_episodes, current.countOfEpisodes.toString()))
    }

    fun setProducts(stores: List<ResultsItem>) {
        mStores = stores
        notifyDataSetChanged()
    }

    inner class SeriesViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var name: TextView? = null
        var numberOfEpisodes: TextView? = null
        var image: ImageView? = null

        init {
            image = itemView.findViewById(R.id.series_image)
            name = itemView.findViewById(R.id.series_name)
            numberOfEpisodes = itemView.findViewById(R.id.series_number_episodes)
            itemView.setOnClickListener {
                mListener?.onItemClick(it, adapterPosition)
            }
        }
    }

    fun setOnItemClickListener(listener: SeriesCallback) {
        mListener = listener
    }

    interface SeriesCallback {
        fun onItemClick(view: View, position: Int)
    }

}