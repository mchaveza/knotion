package com.example.knock.knotion.ui.shop.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.request.RequestOptions
import com.example.knock.knotion.R
import com.example.knock.knotion.data.entity.ShopEntity
import com.ia.mchaveza.kotlin_library.loadUrl
import java.util.*

class ShopAdapter : RecyclerView.Adapter<ShopAdapter.ShopViewHolder>() {

    private var mInflater: LayoutInflater? = null
    private var mStores: List<ShopEntity> = ArrayList()
    private var mListener: WordCallback? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ShopViewHolder {
        mInflater = LayoutInflater.from(parent.context)
        val itemView = mInflater?.inflate(R.layout.item_shop, parent, false)
        return ShopViewHolder(itemView!!)
    }

    override fun getItemCount(): Int =
        mStores.size

    override fun onBindViewHolder(holder: ShopViewHolder, position: Int) = with(holder) {
        val current = mStores[position]
        shopName?.text = current.name
        shopImage?.loadUrl(current.image, RequestOptions().fitCenter())
        shopAddress?.text = current.address
    }

    fun setStores(stores: List<ShopEntity>) {
        mStores = stores
        notifyDataSetChanged()
    }

    inner class ShopViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var shopName: TextView? = null
        var shopAddress: TextView? = null
        var shopImage: ImageView? = null

        init {
            shopName = itemView.findViewById(R.id.shop_name)
            shopAddress = itemView.findViewById(R.id.shop_address)
            shopImage = itemView.findViewById(R.id.shop_image)
            itemView.setOnClickListener {
                mListener?.onItemClick(it, adapterPosition)
            }
        }
    }

    fun setOnItemClickListener(listener: WordCallback) {
        mListener = listener
    }

    interface WordCallback {
        fun onItemClick(view: View, position: Int)
    }

}