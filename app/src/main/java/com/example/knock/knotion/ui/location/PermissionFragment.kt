package com.example.knock.knotion.ui.location

import android.Manifest
import com.example.knock.knotion.R
import com.example.knock.knotion.ui.base.BaseFragment
import com.ia.mchaveza.kotlin_library.PermissionCallback
import com.ia.mchaveza.kotlin_library.PermissionManager
import kotlinx.android.synthetic.main.fragment_permission.*

class PermissionFragment : BaseFragment(), PermissionCallback {

    private var mListener: PermissionFragmentCallback? = null
    private val permissionManager by lazy { PermissionManager(activity!!, this) }

    override fun getLayout(): Int =
        R.layout.fragment_permission

    override fun initView() {
        super.initView()
        setRequestPermissionBtnListener()
    }

    fun setFragmentListener(listener: PermissionFragmentCallback) {
        mListener = listener
    }

    private fun setRequestPermissionBtnListener() {
        this.permission_btn.setOnClickListener {
            permissionManager.requestSinglePermission(Manifest.permission.ACCESS_FINE_LOCATION)
        }
    }

    override fun onPermissionDenied(permission: String) {
    }

    override fun onPermissionGranted(permission: String) {
        mListener?.onDisplayMap()
    }

    interface PermissionFragmentCallback {
        fun onDisplayMap()
    }


}