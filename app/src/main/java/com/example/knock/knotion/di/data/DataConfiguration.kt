package com.example.knock.knotion.di.data

class DataConfiguration(private val baseUrl: String) {

    fun getBaseUrl(): String = baseUrl

    companion object {
        const val SEARCH = "/v1/search"
        const val SERIES = "/api/series_list/"
    }


}