package com.example.knock.knotion.data.net.service

import com.example.knock.knotion.di.data.DataConfiguration
import com.example.knock.knotion.ui.home.model.SeriesResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query
import rx.Observable

interface SeriesService {

    @GET(DataConfiguration.SERIES)
    fun getProducts(
        @Query("api_key") query: String = "b381f398979c82f4e221c53303b9ca3d924689e9",
        @Query("format") format: String = "json"
    ): Observable<Response<SeriesResponse>>

}