package com.example.knock.knotion.ui.home.model

import com.google.gson.annotations.SerializedName

data class SeriesResponse(

    @field:SerializedName("results")
    private val _results: List<ResultsItem>? = null
) {

    val results: List<ResultsItem>
        get() = _results ?: ArrayList()

}