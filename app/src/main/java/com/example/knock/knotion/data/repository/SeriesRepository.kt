package com.example.knock.knotion.data.repository

import com.example.knock.knotion.ui.home.model.SeriesResponse
import rx.Observable

interface SeriesRepository {

    fun getProducts(): Observable<SeriesResponse>

}