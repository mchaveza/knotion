package com.example.knock.knotion.di.database

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import com.example.knock.knotion.data.dao.ShopDao
import com.example.knock.knotion.data.entity.ShopEntity

@Database(entities = [(ShopEntity::class)], version = 1)
abstract class NapifyDatabase : RoomDatabase() {

    abstract fun shopDao(): ShopDao

}