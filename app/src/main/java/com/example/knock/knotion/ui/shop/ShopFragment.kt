package com.example.knock.knotion.ui.shop

import android.arch.lifecycle.Observer
import android.support.v7.widget.LinearLayoutManager
import com.example.knock.knotion.KnotionApplication
import com.example.knock.knotion.R
import com.example.knock.knotion.ui.base.BaseFragment
import com.example.knock.knotion.ui.shop.adapter.ShopAdapter
import com.example.knock.knotion.viewmodels.ShopViewModel
import kotlinx.android.synthetic.main.fragment_shop.*
import javax.inject.Inject

class ShopFragment : BaseFragment() {

    @Inject
    lateinit var mShopViewModel: ShopViewModel

    private val shopAdapter by lazy { ShopAdapter() }

    override fun getLayout(): Int =
        R.layout.fragment_shop

    override fun initView() {
        super.initView()
        setupRecycler()
    }

    private fun setupRecycler() {
        this.shop_list.adapter = shopAdapter
        this.shop_list.layoutManager = LinearLayoutManager(activity!!, LinearLayoutManager.VERTICAL, false)
        this.shop_list.hasFixedSize()
        populateStores()
    }

    private fun populateStores() {
        mShopViewModel.getAllStores().observe(this, Observer { stores ->
            stores?.let {
                if (it.isEmpty()) {
                    mShopViewModel.populateStores()
                } else {
                    shopAdapter.setStores(stores)
                }
            }
        })
    }

    override fun initializeDagger() {
        super.initializeDagger()
        KnotionApplication.getApplicationComponent().inject(this)
    }
}