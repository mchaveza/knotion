package com.example.knock.knotion.domain

import android.arch.lifecycle.LiveData
import android.util.Log
import com.example.knock.knotion.data.dao.ShopDao
import com.example.knock.knotion.data.entity.ShopEntity
import com.example.knock.knotion.di.database.NapifyDatabase
import io.reactivex.Flowable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class ShopInteractor(private val database: NapifyDatabase) {

    private val mShopDao: ShopDao = database.shopDao()
    private val mAllStores: LiveData<List<ShopEntity>> = mShopDao.getAllStores()
    private var subscription: Disposable? = null

    fun getAllStores() = mAllStores

    fun saveStores(stores: List<ShopEntity>) {
        subscription = Flowable.fromArray(stores)
            .subscribeOn(Schedulers.io())
            .observeOn(Schedulers.computation())
            .subscribe({ list ->
                list.forEach {
                    mShopDao.saveStore(it)
                }
            }, {
                Log.e("SaveStore", it.message.toString(), it)
            })
    }

    fun stop() {
        subscription?.dispose()
    }
}