package com.example.knock.knotion.ui.home.model

import com.google.gson.annotations.SerializedName

data class ResultsItem(

    @field:SerializedName("image")
    private val _image: Image? = null,

    @field:SerializedName("aliases")
    private val _aliases: String? = null,

    @field:SerializedName("site_detail_url")
    private val _siteDetailUrl: String? = null,

    @field:SerializedName("first_episode")
    private val _firstEpisode: FirstEpisode? = null,

    @field:SerializedName("deck")
    private val _deck: Any? = null,

    @field:SerializedName("start_year")
    private val _startYear: String? = null,

    @field:SerializedName("description")
    private val _description: String? = null,

    @field:SerializedName("last_episode")
    private val _lastEpisode: LastEpisode? = null,

    @field:SerializedName("api_detail_url")
    private val _apiDetailUrl: String? = null,

    @field:SerializedName("date_added")
    private val _dateAdded: String? = null,

    @field:SerializedName("count_of_episodes")
    private val _countOfEpisodes: Int? = null,

    @field:SerializedName("name")
    private val _name: String? = null,

    @field:SerializedName("publisher")
    private val _publisher: Publisher? = null,

    @field:SerializedName("id")
    private val _id: Int? = null,

    @field:SerializedName("date_last_updated")
    private val _dateLastUpdated: String? = null
) {

    val image: Image
        get() = _image ?: Image()

    val id: Int
        get() = _id ?: -1

    val name: String
        get() = _name ?: ""

    val countOfEpisodes: Int
        get() = _countOfEpisodes ?: 0

    val siteUrl: String
        get() = _siteDetailUrl ?: ""

    val description: String
        get() = _description ?: "Not available at this very moment"

}