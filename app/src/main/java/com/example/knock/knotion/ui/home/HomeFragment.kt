package com.example.knock.knotion.ui.home

import android.arch.lifecycle.Observer
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import com.example.knock.knotion.KnotionApplication
import com.example.knock.knotion.R
import com.example.knock.knotion.ui.base.BaseFragment
import com.example.knock.knotion.ui.home.adapter.SeriesAdapter
import com.example.knock.knotion.ui.products.ProductFragment
import com.example.knock.knotion.viewmodels.SeriesViewModel
import com.ia.mchaveza.kotlin_library.gone
import com.ia.mchaveza.kotlin_library.visible
import kotlinx.android.synthetic.main.fragment_home.*
import javax.inject.Inject

class HomeFragment : BaseFragment() {

    @Inject
    lateinit var mSeriesViewModel: SeriesViewModel

    private val productFragment by lazy { ProductFragment() }
    private val seriesAdapter by lazy { SeriesAdapter() }

    override fun getLayout(): Int =
        R.layout.fragment_home

    override fun initView() {
        super.initView()
        changeToolbarTitle(getString(R.string.home_toolbar_title))
        onLoading()
        setupSeriesRecycler()
        getSeries()
    }

    private fun setupSeriesRecycler() {
        this.home_series.adapter = seriesAdapter
        this.home_series.layoutManager = LinearLayoutManager(activity!!, LinearLayoutManager.VERTICAL, false)
        this.home_series.hasFixedSize()
    }

    private fun getSeries() {
        onLoading()
        onGetSeries()
        onGetSeriesError()
    }

    private fun onLoading() {
        mSeriesViewModel.getLoadingVisibility().observe(this, Observer {
            it?.let { loading ->
                if (loading) {
                    this.home_loading.visible()
                } else {
                    this.home_loading.gone()
                }
            }
        })
    }

    private fun onGetSeries() {
        mSeriesViewModel.getSeries().observe(this, Observer {
            it?.let { series ->
                seriesAdapter.setProducts(series)
            }
        })
    }

    private fun onGetSeriesError() {
        mSeriesViewModel.getSeriesError().observe(this, Observer {
            it?.let { error ->
                Log.e("12", "12")
            }
        })
    }

    override fun initializeDagger() {
        super.initializeDagger()
        KnotionApplication.getApplicationComponent().inject(this)
    }

    private fun changeToolbarTitle(title: String) {
        (activity as HomeActivity).changeToolbarTitle(title)
    }

    fun performSearching(query: String) {
        productFragment.performSearching(query)
    }
}