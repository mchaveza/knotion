package com.example.knock.knotion.ui.views

interface BaseView {
    fun showLoading()
    fun hideLoading()
}