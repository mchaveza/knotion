package com.example.knock.knotion.ui.location

import android.annotation.SuppressLint
import android.location.Location
import android.os.Bundle
import android.view.View
import com.example.knock.knotion.KnotionApplication
import com.example.knock.knotion.R
import com.example.knock.knotion.ui.base.BaseFragment
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.LatLng
import com.ia.mchaveza.kotlin_library.TrackingManagerLocationCallback
import com.ia.mchaveza.kotlin_library.setCurrentPosition
import com.ia.mchaveza.kotlin_library.setDaylightStyle
import kotlinx.android.synthetic.main.fragment_maps.*

class MapsFragment : BaseFragment() {

    private var mGoogleMap: GoogleMap? = null

    override fun getLayout(): Int =
        R.layout.fragment_maps

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupMap(savedInstanceState)
    }

    private fun setupLocationUpdates() {
        trackingManager.startLocationUpdates(object : TrackingManagerLocationCallback {
            override fun onLocationHasChanged(location: Location) {
                trackingManager.stopLocationUpdates()
                mGoogleMap?.setCurrentPosition(LatLng(location.latitude, location.longitude), 14f)
            }

            override fun onLocationHasChangedError(error: Exception) {
            }
        })
    }

    private fun setupMap(savedInstanceState: Bundle?) {
        this.location_map.onCreate(savedInstanceState)
        this.location_map.onResume()
        this.location_map.getMapAsync {
            mGoogleMap = it
            mGoogleMap?.setDaylightStyle(activity!!)
            enableMapLocation(true)
            setupLocationUpdates()
        }
    }

    override fun initializeDagger() {
        super.initializeDagger()
        KnotionApplication.getApplicationComponent().inject(this)
    }

    override fun onStop() {
        super.onStop()
        enableMapLocation(false)
    }


    override fun onResume() {
        super.onResume()
        enableMapLocation(true)
    }

    //Safe here because we check it before displaying this fragment
    @SuppressLint("MissingPermission")
    private fun enableMapLocation(enable: Boolean) {
        mGoogleMap?.isMyLocationEnabled = enable
    }
}