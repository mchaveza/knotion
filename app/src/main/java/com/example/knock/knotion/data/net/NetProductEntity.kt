package com.example.knock.knotion.data.net

import com.example.knock.knotion.data.net.service.ProductService
import com.example.knock.knotion.data.repository.ProductsRepository
import com.example.knock.knotion.ui.products.models.ProductResponse
import rx.Observable

class NetProductEntity(private val productService: ProductService) : ProductsRepository {

    override fun search(query: String): Observable<ProductResponse> =
        productService.getProducts(query)
            .flatMap {
                if (it.isSuccessful) {
                    Observable.just(it.body())
                } else {
                    Observable.error(Throwable(it.message()))
                }
            }

}