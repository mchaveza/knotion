package com.example.knock.knotion.ui.home.model

import com.google.gson.annotations.SerializedName

data class Publisher(

    @field:SerializedName("api_detail_url")
    val apiDetailUrl: String? = null,

    @field:SerializedName("name")
    val name: String? = null,

    @field:SerializedName("id")
    val id: Int? = null
)