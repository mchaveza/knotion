package com.example.knock.knotion.domain

import com.example.knock.knotion.data.repository.SeriesRepository
import com.example.knock.knotion.ui.home.model.ResultsItem
import rx.Subscription
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers

class SeriesInteractor(private val repository: SeriesRepository) {

    private var mListener: SeriesCallback? = null
    private var subscription: Subscription? = null

    fun setOnSeriesListener(listener: SeriesCallback) {
        mListener = listener
    }

    fun getSeries() {
        subscription = repository.getProducts()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                mListener?.onGetSeries(it.results)
            }, {
                mListener?.onGetSeriesError(it)
            })
    }

    fun stop() {
        subscription?.unsubscribe()
    }

    interface SeriesCallback {
        fun onGetSeries(series: List<ResultsItem>)
        fun onGetSeriesError(error: Throwable)
    }

}