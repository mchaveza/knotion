package com.example.knock.knotion.ui.home.adapter

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import com.example.knock.knotion.ui.location.LocationFragment
import com.example.knock.knotion.ui.location.MapsFragment
import com.example.knock.knotion.ui.products.ProductFragment
import com.example.knock.knotion.ui.shop.ShopFragment

class HomePagerAdapter(fm: FragmentManager, productFragment: ProductFragment) : FragmentPagerAdapter(fm) {

    private val mFragmentList = mutableListOf<Fragment>()

    init {
        mFragmentList.clear()
        mFragmentList.add(ShopFragment())
        mFragmentList.add(productFragment)
        mFragmentList.add(LocationFragment())
    }

    override fun getItem(position: Int): Fragment {
        return mFragmentList[position]
    }

    override fun getCount() = mFragmentList.size

}