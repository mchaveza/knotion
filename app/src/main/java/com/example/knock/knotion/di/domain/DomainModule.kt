package com.example.knock.knotion.di.domain

import com.example.knock.knotion.data.repository.ProductsRepository
import com.example.knock.knotion.data.repository.SeriesRepository
import com.example.knock.knotion.di.database.NapifyDatabase
import com.example.knock.knotion.domain.ProductInteractor
import com.example.knock.knotion.domain.SeriesInteractor
import com.example.knock.knotion.domain.ShopInteractor
import dagger.Module
import dagger.Provides

@Module
class DomainModule {

    @Provides
    fun providesShopInteractor(database: NapifyDatabase) =
        ShopInteractor(database)

    @Provides
    fun providesProductsInteractor(repository: ProductsRepository) =
        ProductInteractor(repository)

    @Provides
    fun providesSeriesInteractor(repository: SeriesRepository) =
        SeriesInteractor(repository)

}