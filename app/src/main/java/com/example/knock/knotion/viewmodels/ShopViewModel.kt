package com.example.knock.knotion.viewmodels

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.ViewModel
import com.example.knock.knotion.data.entity.ShopEntity
import com.example.knock.knotion.domain.ShopInteractor
import javax.inject.Inject

class ShopViewModel
@Inject
constructor(private val shopInteractor: ShopInteractor) : ViewModel() {

    private val mAllStores: LiveData<List<ShopEntity>> =
        shopInteractor.getAllStores()

    fun getAllStores() = mAllStores

    fun populateStores() {
        val stores = mutableListOf<ShopEntity>()
        stores.add(
            ShopEntity(
                0,
                "Amazon",
                "http://g-ec2.images-amazon.com/images/G/01/social/api-share/amazon_logo_500500._V323939215_.png",
                "Seattle, Estados Unidos, Washington"
            )
        )
        stores.add(
            ShopEntity(
                1,
                "Liverpool",
                "https://cdn.shopify.com/s/files/1/0899/2262/files/Liverpool_medium.png?98663997157997807",
                "Av. Lic. Enrique Ramírez Miguel 1000"
            )
        )
        stores.add(
            ShopEntity(
                2,
                "Cuidado con el perro",
                "https://s3-media2.fl.yelpcdn.com/bphoto/x066TFRDF6elp_yEn_3q0g/o.jpg",
                "Av Francisco I. Madero Ote 73, Centro Histórico, 58000 Morelia, Mich."
            )
        )
        stores.add(
            ShopEntity(
                3,
                "H&M",
                "https://upload.wikimedia.org/wikipedia/commons/thumb/5/53/H%26M-Logo.svg/245px-H%26M-Logo.svg.png",
                "Av Montaña Monarca 1000, 58350 Morelia, Mich."
            )
        )
        stores.add(
            ShopEntity(
                4,
                "Soriana",
                "https://upload.wikimedia.org/wikipedia/commons/thumb/6/61/LogoSorianaSVG.svg/300px-LogoSorianaSVG.svg.png",
                "Av Solidaridad 350, Félix Ireta, 58070 Morelia, Mich."
            )
        )
        saveStores(stores)
    }

    private fun saveStores(stores: List<ShopEntity>) {
        shopInteractor.saveStores(stores)
    }

    override fun onCleared() {
        super.onCleared()
        shopInteractor.stop()
    }

}