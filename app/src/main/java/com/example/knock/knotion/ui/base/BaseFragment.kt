package com.example.knock.knotion.ui.base

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.knock.knotion.KnotionApplication
import com.ia.mchaveza.kotlin_library.SharedPreferencesManager
import com.ia.mchaveza.kotlin_library.TrackingManager
import com.ia.mchaveza.kotlin_library.inflate
import javax.inject.Inject

abstract class BaseFragment : Fragment() {

    @Inject
    lateinit var preferences: SharedPreferencesManager

    @Inject
    lateinit var trackingManager: TrackingManager

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return container?.inflate(getLayout())
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
    }

    abstract fun getLayout(): Int

    open fun initView() {
        initializeDagger()
        trackingManager.enablePermissionSetup(activity!!)
    }

    open fun initializeDagger() {
        KnotionApplication.getApplicationComponent().inject(this)
    }
}