package com.example.knock.knotion.ui.products.models

import com.google.gson.annotations.SerializedName

data class ItemsItem(

    @field:SerializedName("largeImage")
    private val _largeImage: String? = null,

    @field:SerializedName("mediumImage")
    private val _mediumImage: String? = null,

    @field:SerializedName("salePrice")
    private val _salePrice: Double? = null,

    @field:SerializedName("shortDescription")
    private val _shortDescription: String? = null,

    @field:SerializedName("itemId")
    private val _itemId: Int? = null,

    @field:SerializedName("name")
    private val _name: String? = null

) {

    val image: String
        get() = _mediumImage ?: _mediumImage ?: ""

    val name: String
        get() = _name ?: ""

    val price: Double
        get() = _salePrice ?: 0.0

    val description: String
        get() = _shortDescription ?: "ND"


}