package com.example.knock.knotion.data.repository

import com.example.knock.knotion.ui.products.models.ProductResponse
import rx.Observable

interface ProductsRepository {
    fun search(query: String): Observable<ProductResponse>
}