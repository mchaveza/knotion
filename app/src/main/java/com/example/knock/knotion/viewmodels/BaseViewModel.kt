package com.example.knock.knotion.viewmodels

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel

open class BaseViewModel : ViewModel() {

    private val loadingVisibility = MutableLiveData<Boolean>()

    fun showLoading() {
        loadingVisibility.value = true
    }

    fun hideLoading() {
        loadingVisibility.value = false
    }

    fun getLoadingVisibility() = loadingVisibility

}