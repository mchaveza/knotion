package com.example.knock.knotion.di.main.modules

import android.content.Context
import com.example.knock.knotion.KnotionApplication
import com.ia.mchaveza.kotlin_library.SharedPreferencesManager
import com.ia.mchaveza.kotlin_library.TrackingManager
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class ApplicationModule(private val application: KnotionApplication) {

    @Provides
    @Singleton
    fun providesApplication(): KnotionApplication = application

    @Provides
    @Singleton
    fun providesContext(): Context = application.applicationContext

    @Provides
    @Singleton
    fun providesPreferencesHelper(context: Context): SharedPreferencesManager = SharedPreferencesManager(context)

    @Provides
    @Singleton
    fun providesTrackingManager(context: Context): TrackingManager = TrackingManager(context)
}