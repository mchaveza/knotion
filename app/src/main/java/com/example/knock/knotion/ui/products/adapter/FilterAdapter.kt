package com.example.knock.knotion.ui.products.adapter

import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import com.example.knock.knotion.R
import com.ia.mchaveza.kotlin_library.inflate

class FilterAdapter : BaseAdapter() {

    private val filterList = mutableListOf<String>()

    init {
        filterList.add("Nombre")
        filterList.add("Precio")
    }

    override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup): View {
        return createItemView(position, parent)
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        return createItemView(position, parent)
    }

    private fun createItemView(position: Int, parent: ViewGroup): View {
        val view = parent.inflate(R.layout.item_filter)
        val filterName = view.findViewById<TextView>(R.id.filter_name)
        filterName.text = filterList[position]
        return view
    }

    override fun getItem(position: Int): String? =
        filterList[position]

    override fun getItemId(position: Int): Long = position.toLong()

    override fun getCount(): Int = filterList.size
}

