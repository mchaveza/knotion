package com.example.knock.knotion.viewmodels

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import com.example.knock.knotion.domain.SeriesInteractor
import com.example.knock.knotion.ui.home.model.ResultsItem
import javax.inject.Inject

class SeriesViewModel
@Inject
constructor(private val seriesInteractor: SeriesInteractor) : BaseViewModel(), SeriesInteractor.SeriesCallback {

    private val mSeries = MutableLiveData<List<ResultsItem>>()
    private val mError = MutableLiveData<Throwable>()

    init {
        seriesInteractor.setOnSeriesListener(this)
    }

    fun getSeries(): LiveData<List<ResultsItem>> {
        showLoading()
        seriesInteractor.getSeries()
        return mSeries
    }

    fun getSeriesError(): LiveData<Throwable> = mError

    override fun onGetSeries(series: List<ResultsItem>) {
        hideLoading()
        mSeries.value = series

    }

    override fun onGetSeriesError(error: Throwable) {
        hideLoading()
        mError.value = error
    }

    override fun onCleared() {
        super.onCleared()
        seriesInteractor.stop()
    }

}