package com.example.knock.knotion.domain

import com.example.knock.knotion.data.repository.ProductsRepository
import com.example.knock.knotion.ui.products.models.ItemsItem
import rx.Subscription
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers

class ProductInteractor(private val repository: ProductsRepository) {

    private var mListener: ProductCallback? = null
    private var subscription: Subscription? = null

    fun setOnProductListener(listener: ProductCallback) {
        mListener = listener
    }

    fun getProducts(query: String) {
        subscription =
                repository.search(query)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({
                        mListener?.onGetProducts(it.products)
                    }, {
                        mListener?.onProductsError(it)
                    })
    }

    fun stop() {
        subscription?.unsubscribe()
    }

    interface ProductCallback {
        fun onGetProducts(products: List<ItemsItem>)
        fun onProductsError(error: Throwable)
    }

}