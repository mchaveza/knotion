package com.example.knock.knotion.ui.home.model

import com.google.gson.annotations.SerializedName

data class Image(

    @field:SerializedName("icon_url")
    private val _iconUrl: String? = null,

    @field:SerializedName("screen_large_url")
    private val _screenLargeUrl: String? = null,

    @field:SerializedName("thumb_url")
    private val _thumbUrl: String? = null,

    @field:SerializedName("tiny_url")
    private val _tinyUrl: String? = null,

    @field:SerializedName("small_url")
    private val _smallUrl: String? = null,

    @field:SerializedName("super_url")
    private val _superUrl: String? = null,

    @field:SerializedName("original_url")
    private val _originalUrl: String? = null,

    @field:SerializedName("screen_url")
    private val _screenUrl: String? = null,

    @field:SerializedName("medium_url")
    private val _mediumUrl: String? = null,

    @field:SerializedName("image_tags")
    private val _imageTags: String? = null
) {

    val imageUrl: String
        get() = _mediumUrl ?: ""


}