package com.example.knock.knotion.ui.products.models

import com.google.gson.annotations.SerializedName

data class ProductResponse(

    @field:SerializedName("query")
    private val _query: String? = null,

    @field:SerializedName("items")
    private val _items: List<ItemsItem>? = null

) {

    val query: String
        get() = _query ?: "None"

    val products: List<ItemsItem>
        get() = _items ?: listOf()

}