package com.example.knock.knotion

import android.support.multidex.MultiDexApplication
import com.example.knock.knotion.di.data.DataNetModule
import com.example.knock.knotion.di.main.components.ApplicationComponent
import com.example.knock.knotion.di.main.components.DaggerApplicationComponent
import com.example.knock.knotion.di.main.modules.ApplicationModule
import com.facebook.stetho.Stetho

class KnotionApplication : MultiDexApplication() {

    companion object {
        private lateinit var applicationComponent: ApplicationComponent

        private fun initDependencies(application: KnotionApplication) {
            applicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(ApplicationModule(application))
                .dataNetModule(DataNetModule(application.getString(R.string.base_url)))
                .build()
        }

        fun getApplicationComponent() = applicationComponent
    }

    override fun onCreate() {
        super.onCreate()
        initDependencies(this)
        Stetho.initializeWithDefaults(this)
    }

}