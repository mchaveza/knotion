package com.example.knock.knotion.viewmodels

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.example.knock.knotion.domain.ProductInteractor
import com.example.knock.knotion.ui.products.models.ItemsItem
import com.example.knock.knotion.ui.views.BaseView
import javax.inject.Inject

class ProductViewModel
@Inject
constructor(private val productInteractor: ProductInteractor) : ViewModel(), ProductInteractor.ProductCallback {

    private var mView: BaseView? = null
    private val mProducts = MutableLiveData<List<ItemsItem>>()

    init {
        productInteractor.setOnProductListener(this)
    }

    fun setView(view: BaseView) {
        mView = view
        productInteractor.setOnProductListener(this)
    }

    fun getProducts(query: String): LiveData<List<ItemsItem>> {
        mView?.showLoading()
        productInteractor.getProducts(query)
        return mProducts
    }

    override fun onGetProducts(products: List<ItemsItem>) {
        mView?.hideLoading()
        mProducts.value = products
        sortProductList(ProductFilter.Name)
    }

    override fun onProductsError(error: Throwable) {
        mView?.hideLoading()
        mProducts.value = mutableListOf()
    }

    override fun onCleared() {
        super.onCleared()
        productInteractor.stop()
    }

    fun sortProductList(filter: ProductFilter) {
        mProducts.value = when (filter) {
            ProductFilter.Name -> {
                mProducts.value?.sortedBy { it.name }
            }
            ProductFilter.Price -> {
                mProducts.value?.sortedBy { it.price }
            }
        }
    }
}

enum class ProductFilter {
    Name,
    Price
}