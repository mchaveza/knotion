package com.example.knock.knotion.ui.products.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.request.RequestOptions
import com.example.knock.knotion.R
import com.example.knock.knotion.ui.products.models.ItemsItem
import com.ia.mchaveza.kotlin_library.loadUrl
import java.util.*

class ProductAdapter : RecyclerView.Adapter<ProductAdapter.ProductViewHolder>() {

    private var mInflater: LayoutInflater? = null
    private var mStores: List<ItemsItem> = ArrayList()
    private var mListener: ProductCallback? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductViewHolder {
        mInflater = LayoutInflater.from(parent.context)
        val itemView = mInflater?.inflate(R.layout.item_product, parent, false)
        return ProductViewHolder(itemView!!)
    }

    override fun getItemCount(): Int =
        mStores.size

    override fun onBindViewHolder(holder: ProductViewHolder, position: Int) = with(holder) {
        val current = mStores[position]
        productName?.text = current.name
        productPrice?.text = String.format("$%s", current.price)
        productImage?.loadUrl(current.image, RequestOptions().fitCenter())
        productDescription?.text = current.description
    }

    fun setProducts(stores: List<ItemsItem>) {
        mStores = stores
        notifyDataSetChanged()
    }

    inner class ProductViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var productName: TextView? = null
        var productPrice: TextView? = null
        var productDescription: TextView? = null
        var productImage: ImageView? = null

        init {
            productImage = itemView.findViewById(R.id.product_image)
            productName = itemView.findViewById(R.id.product_name)
            productPrice = itemView.findViewById(R.id.product_price)
            productDescription = itemView.findViewById(R.id.product_description)
            itemView.setOnClickListener {
                mListener?.onItemClick(it, adapterPosition)
            }
        }
    }

    fun setOnItemClickListener(listener: ProductCallback) {
        mListener = listener
    }

    interface ProductCallback {
        fun onItemClick(view: View, position: Int)
    }

}