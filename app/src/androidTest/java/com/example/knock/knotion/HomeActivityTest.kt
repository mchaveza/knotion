package com.example.knock.knotion

import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.action.ViewActions
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.matcher.ViewMatchers.*
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import android.view.View
import android.view.ViewGroup
import com.example.knock.knotion.ui.home.HomeActivity
import org.hamcrest.Description
import org.hamcrest.Matcher
import org.hamcrest.Matchers
import org.hamcrest.TypeSafeMatcher
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class HomeActivityTest {

    @Rule
    @JvmField
    var mActivityTestRule = ActivityTestRule(HomeActivity::class.java)

    @Test
    fun checkStoreToolbarTitle() {
        val toolbarTitle = mActivityTestRule.activity.applicationContext.getString(R.string.navigation_shop)
        val textView = onView(
            Matchers.allOf(
                withText(toolbarTitle),
                childAtPosition(
                    Matchers.allOf(
                        withId(R.id.base_toolbar),
                        childAtPosition(
                            withId(R.id.base_appbar),
                            0
                        )
                    ),
                    0
                ),
                isDisplayed()
            )
        )
        textView.check(matches(withText(toolbarTitle)))
    }

    @Test
    fun checkProductToolbarTitle() {
        val toolbarTitle = mActivityTestRule.activity.applicationContext.getString(R.string.navigation_product)
        val bottomNavigationItemView = onView(
            Matchers.allOf(
                withId(R.id.navigation_products), withContentDescription(toolbarTitle),
                childAtPosition(
                    childAtPosition(
                        withId(R.id.home_navigation),
                        0
                    ),
                    1
                ),
                isDisplayed()
            )
        )
        bottomNavigationItemView.perform(ViewActions.click())

        val viewPager = onView(
            Matchers.allOf(
                withId(R.id.home_viewpager),
                childAtPosition(
                    childAtPosition(
                        withId(R.id.base_container),
                        0
                    ),
                    0
                ),
                isDisplayed()
            )
        )
        viewPager.perform(ViewActions.swipeLeft())

        val textView = onView(
            Matchers.allOf(
                withText(toolbarTitle),
                childAtPosition(
                    Matchers.allOf(
                        withId(R.id.base_toolbar),
                        childAtPosition(
                            withId(R.id.base_appbar),
                            0
                        )
                    ),
                    0
                ),
                isDisplayed()
            )
        )
        textView.check(matches(withText(toolbarTitle)))
    }

    @Test
    fun checkLocationToolbarTitle() {
        val toolbarTitle = mActivityTestRule.activity.applicationContext.getString(R.string.navigation_location)
        val bottomNavigationItemView = onView(
            Matchers.allOf(
                withId(R.id.navigation_location), withContentDescription(toolbarTitle),
                childAtPosition(
                    childAtPosition(
                        withId(R.id.home_navigation),
                        0
                    ),
                    2
                ),
                isDisplayed()
            )
        )
        bottomNavigationItemView.perform(ViewActions.click())

        val viewPager = onView(
            Matchers.allOf(
                withId(R.id.home_viewpager),
                childAtPosition(
                    childAtPosition(
                        withId(R.id.base_container),
                        0
                    ),
                    0
                ),
                isDisplayed()
            )
        )
        viewPager.perform(ViewActions.swipeLeft())

        val textView = onView(
            Matchers.allOf(
                withText(toolbarTitle),
                childAtPosition(
                    Matchers.allOf(
                        withId(R.id.base_toolbar),
                        childAtPosition(
                            withId(R.id.base_appbar),
                            0
                        )
                    ),
                    0
                ),
                isDisplayed()
            )
        )
        textView.check(matches(withText(toolbarTitle)))
    }

    private fun childAtPosition(
        parentMatcher: Matcher<View>, position: Int
    ): Matcher<View> {

        return object : TypeSafeMatcher<View>() {
            override fun describeTo(description: Description) {
                description.appendText("Child at position $position in parent ")
                parentMatcher.describeTo(description)
            }

            public override fun matchesSafely(view: View): Boolean {
                val parent = view.parent
                return parent is ViewGroup && parentMatcher.matches(parent)
                        && view == parent.getChildAt(position)
            }
        }
    }
}